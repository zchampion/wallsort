from PIL import Image

import argparse
import os
import yaml
from random import randint


def sort_wallpapers(directory, categories=False, norename=False):
    """
    TODO: Flesh out this description
    :param norename:
    :param directory:
    :param categories:
    :return:
    """
    config = yaml.safe_load(open('config.yaml'))
    VALID_EXTENSIONS = tuple(config['valid extensions'])

    wallpaper_filenames = [filename for filename in os.listdir(directory) if filename.endswith(VALID_EXTENSIONS)]
    wallpapers_by_aspect_ratio = {}
    # Make lists for each ratio and sort the filenames according to the ratios
    for wallpaper in wallpaper_filenames:
        print(f'Examining {wallpaper}...', end='\r')
        try:
            image = Image.open(os.path.join(directory, wallpaper))

            # Sort the wallpapers by ratio in the dictionary
            width, height = image.size
            ratio = '{:3.1f}'.format(width / height)

            if ratio in wallpapers_by_aspect_ratio:
                wallpapers_by_aspect_ratio[ratio].append(wallpaper)
            else:
                wallpapers_by_aspect_ratio[ratio] = [wallpaper]

        except IOError as e:
            print(f'Quarantining {wallpaper} due to {type(e).__name__}: {e}...')
            os.rename(os.path.join(directory, wallpaper),
                      os.path.join('/home/queen/Desktop/', 'wallpaper_quarantine', wallpaper))
    print('\33[2KExamining done!')

    if categories:
        # Make the directories for the categories
        for category in config['categories']:
            ensure_directory_exists(directory, category)
        print('\33[2KCreating dirs done!')

        for category in config['categories']:
            min_ratio = config['categories'][category]['min']
            max_ratio = config['categories'][category]['max']
            category_ratios = [str(r / 10) for r in range(int(min_ratio * 10), int(max_ratio * 10) + 1)
                               if str(r / 10) in wallpapers_by_aspect_ratio]

            for ratio in category_ratios:
                for wallpaper_file in wallpapers_by_aspect_ratio[ratio]:
                    if not norename:
                        new_filename = generate_new_random_filename(wallpaper_file)
                    else:
                        new_filename = wallpaper_file
                    print(f'Sorting {wallpaper_file} into dir {category} as {new_filename}...', end='\r')
                    os.rename(os.path.join(directory, wallpaper_file),
                              os.path.join(directory, category, new_filename))
        print('\33[2KSorting done!')

    else:
        # Make the directories for the aspect ratios
        for ratio in wallpapers_by_aspect_ratio:
            ensure_directory_exists(directory, ratio)
        print('\33[2KCreating dirs done!')

        # Move the wallpapers into the folders according to their entries in the dictionary
        for wallpaper_ratio in wallpapers_by_aspect_ratio:
            for wallpaper_file in wallpapers_by_aspect_ratio[wallpaper_ratio]:
                print(f'Sorting file {wallpaper_file} into dir {wallpaper_ratio}...', end='\r')
                os.rename(os.path.join(directory, wallpaper_file),
                          os.path.join(directory, str(wallpaper_ratio), wallpaper_file))
        print('\33[2KSorting done!')


def ensure_directory_exists(base_directory, new_directory_name):
    print(f'Creating dir {new_directory_name}...', end='\r')
    try:
        os.makedirs(os.path.join(base_directory, str(new_directory_name)))
        print(f'\33[2KCreated directory {new_directory_name} in {base_directory}.')
    except FileExistsError:
        pass


def generate_new_random_filename(old_filename):
    extension = old_filename.split('.')[-1]
    return '{rand1}-{rand2}-{rand3}.{extension}'.format(rand1=str(randint(100000, 1000000)),
                                                        rand2=str(randint(100000, 1000000)),
                                                        rand3=str(randint(100000, 1000000)),
                                                        extension=extension)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', nargs='?', default=os.getcwd(),
                        help='The directory in which the script will look for wallpapers'
                             ' to sort and put the resulting sorted subdirectories.')
    parser.add_argument('--categories', action='store_true',
                        help='Sort the wallpapers into the categories specified in'
                             ' config.yaml instead of by raw width to height ratio.')
    parser.add_argument('--norename', action='store_true',
                        help='If set, will not rename the wallpapers as it sorts. Otherwise,'
                             'the default is to rename randomly.')
    args = parser.parse_args()

    sort_wallpapers(directory=args.dir, categories=args.categories, norename=args.norename)
